const express = require("express");

class User {
    constructor( id, name, position, office, age, startDate) {
        this.id = id,
        this.name = name,
        this.position = position,
        this.office = office,
        this.age = age,
        this.startDate = startDate
    }
}

let person1 = new User(1, "Airi Satou", "Accountant", "Tokyo", 33, "2008/11/28");
let person2 = new User(2, "Chief Executive Officer (CEO)", "Accountant", "London", 47, "2009/10/09");
let person3 = new User(3, "Ashton Cox", "Junior Technical Author", "San Francisco", 66, "2009/01/12");
let person4 = new User(4, "Bradley Greer", "Software Engineer", "London", 41, "2012/10/13");
let person5 = new User(5, "Brenden Wagner", "Software Engineer", "San Francisco", 28, "2011/06/07");
let person6 = new User(6, "Brielle Williamson", "Integration Specialist", "New York", 61, "2012/12/02");
let person7 = new User(7, "Bruno Nash", "Software Engineer", "London", 38, "2011/05/03");
let person8 = new User(8, "Caesar Vance", "Pre-Sales Support", "New York", 21, "2011/12/12");
let person9 = new User(9, "Cara Stevens", "Sales Assistant", "New York", 46, "2011/12/06");
let person10 = new User(10, "Cedric Kelly", "Senior Javascript Developer", "Edinburgh", 22, "2012/03/29");

var users = [];
users.push(person1, person2, person3, person4, person5, person6, person7, person8, person9, person10);

module.exports = {users};