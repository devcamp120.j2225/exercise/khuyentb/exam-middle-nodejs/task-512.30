// Import thư viện express
const express = require('express');
const { userMiddleware } = require('../Middleware/middleware');
const { users } = require('../../data');

// Tạo router
const userRouter = express.Router();

// Sử dụng middleware
userRouter.use(userMiddleware);

// Get all users
userRouter.get('/users', (req, res) => {
     let age = req.query.age;
     age = parseInt(age);
     if (age) {
          res.status(200).json({
               users: users.filter((items) => items.age > age)
          })
     }
     if (!Number.isInteger(age)) {
          res.status(400).json({
               message: 'Age must be a number'
          })
     } else {
          res.status(200).json({
               users: users
          })
     }
})

// Get an user
userRouter.get('/users/:userID', (req, res) => {
     let id = req.params.userID;
     id = parseInt(id);
     if (!Number.isInteger(id)) {
          return res.status(400).json({
               message: 'User ID is invalid!'
          })
     } else {
          return res.status(200).json({
               message: `Get an user with ID ${id} successfully`,
               user: users.filter((item) => item.id === id)
          })
     }
})

// Create an user
userRouter.post('/users', (req, res) => {
     let body = req.body;
     res.status(201).json({
          message: 'Create a new user successfully',
          ...body
     })
})

// Update an user
userRouter.put('/users/:userID', (req, res) => {
     let id = req.params.userID;
     let body = req.body;
     id = parseInt(id);
     if (!id) {
          res.status(400).json({
               message: 'User ID is invalid!'
          })
     }
     return res.status(200).json({
          message: `Update an user with ID ${id} successfully`,
          updatedUser: { id, ...body }
     })
})

// Delete an user
userRouter.delete('/users/:userID', (req, res) => {
     let id = req.params.userID;
     if (!id) {
          res.status(400).json({
               message: 'User ID is invalid!'
          })
     }
     return res.status(204).json({
          message: `Delete an user with ID ${id} successfully`
     })
})

module.exports = { userRouter };
